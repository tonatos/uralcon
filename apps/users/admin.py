from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.utils.html import format_html
from django.db.models import Q

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import User, Children


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        exclude = ('password', 'last_login', 'is_superuser', 'groups', 'user_permissions', 'is_staff', 'is_active')
        fields = ('id', 'first_name', 'last_name', 'date_joined', 'email', 'vk_id', 'birth_date', 'city',
                  'children_info', 'addition_info')


class UserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class UserOrderFilter(admin.SimpleListFilter):
    title = 'Есть заказ или группа'
    parameter_name = 'without_order'

    def lookups(self, request, model_admin):
        return (
            (1, 'Да'),
            (0, 'Нет'),
        )

    def queryset(self, request, queryset):
        queryset = queryset.select_related()
        if self.value() is '1':
            return queryset.filter(Q(order__isnull=False)|Q(placement_users__isnull=False)).distinct()
        if self.value() is '0':
            return queryset.filter(order__isnull=True, placement_users__isnull=True).distinct()


class UserAdmin(ImportExportModelAdmin, UserAdmin):
    resource_class = UserResource
    form = UserChangeForm
    list_display = ('email', 'first_name', 'last_name', 'city', 'is_staff')
    list_filter = ('is_staff', 'birth_date', 'city', UserOrderFilter)
    
    fieldsets = (
        (None, {'fields': ('password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Прочие данные'), {
            'fields': (
                'vk_id', 'nickname', 'birth_date', 'city', 
                'photo', 'charge_person', 'children_info',
                'addition_info', 'discount'
            )
        },),
        (_('Данные о заказе'), {
            'fields': (
                'total_amount', 'room_amount', 'eat_amount', 'order_link'
            )
        },)
    )

    add_fieldsets = (
        (None, {
                'classes': ('wide',),
                'fields': ('email', 'password1', 'password2')
            }
        ),
    )
    search_fields = ('email', 'city', 'first_name', 'last_name')
    readonly_fields = ('total_amount', 'room_amount', 'eat_amount', 'order_link')
    ordering = ('last_name', 'email', 'birth_date', 'city')
    filter_horizontal = ()

    def total_amount(self, obj):
        return obj.get_total_amount()
    total_amount.short_description = 'Cумма'

    def room_amount(self, obj):
        return obj.get_room_amount()
    room_amount.short_description = 'Размещение'

    def eat_amount(self, obj):
        return obj.get_eat_amount()
    eat_amount.short_description = 'Еда'

    def order_link(self, obj):
        if obj.get_order():
            return format_html('<a href="{url}">{title}</a>',
                url=reverse('admin:order_order_change', args=(obj.get_order()['order'].pk,)),
                title=obj.get_order()['order']
            )
        else:
            return False
    order_link.allow_tags = True
    order_link.short_description = 'Ссылка на заказ'


class ChildrenAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Children, ChildrenAdmin)