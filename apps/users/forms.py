from datetime import datetime, timedelta
from django import forms
from .models import User


def is_18(birth_date):
    return False if not birth_date else birth_date + timedelta(days=365 * 18) < datetime.date( datetime.now() )


class UserRegistrationForm(forms.ModelForm):
    birth_date = forms.DateField(label='Дата рождения', widget=forms.DateInput())
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput())
    password2 = forms.CharField(label='Подтвердите пароль', widget=forms.PasswordInput())
    has_children = forms.BooleanField(label='Везу с собой детей',
                                      widget=forms.CheckboxInput(), required=False)
    bring_doc_from_parents = forms.BooleanField(label='Принесу доверенность от родителей',
                                                widget=forms.CheckboxInput(), required=False)
    accept_personal_data = forms.BooleanField(label='Даю согласие на обработку персональных данных',
                                              widget=forms.CheckboxInput())
    accept_rules = forms.BooleanField(label='С правилами фестиваля ознакомлен, готов соблюдать',
                                      widget=forms.CheckboxInput())
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'vk_id', 'nickname', 'birth_date', 'city', 'photo',
                  'charge_person', 'children_info', 'password1', 'password2', 'bring_doc_from_parents',
                  'accept_personal_data', 'accept_rules']

    def clean(self):
        cleaned_data = super(UserRegistrationForm, self).clean()

        if cleaned_data.get("password1") != cleaned_data.get("password2"):
            raise forms.ValidationError('Пароли не совпадают', code='invalid',)

        if is_18(cleaned_data.get('birth_date')):
            if cleaned_data.get('has_children') and cleaned_data.get('children_info') is '':
                raise forms.ValidationError(
                    'Если вы везете с собой детей, то вы должны указать информацию о них', code='invalid', )
        else:
            if cleaned_data.get('bring_doc_from_parents') is None:
                raise forms.ValidationError(
                    'Если вам меньше 18 лет, вы обязаны принести письменное свидетельство того, что ваши родители '
                    'не против вашего присутствия', code='invalid', )

            if cleaned_data.get('charge_person') is None:
                raise forms.ValidationError(
                    'Вы должны выбрать человека, который будет ответственен за вас в течении мероприятия',
                    code='invalid', )


class UserProfileForm(UserRegistrationForm):
    password1 = None
    password2 = None
    accept_personal_data = None
    accept_rules = None

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'vk_id', 'nickname', 'birth_date', 'city', 'photo',
                  'charge_person', 'children_info', 'bring_doc_from_parents',]