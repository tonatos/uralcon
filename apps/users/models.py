from functools import reduce

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse

from dynamic_preferences.registries import global_preferences_registry


global_preferences = global_preferences_registry.manager()


class UserManager(BaseUserManager):
    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The Email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField('Эл. почта', max_length=255, unique=True,)
    vk_id = models.CharField('ВКонтакте ID', max_length=50, blank=True)
    nickname = models.CharField('Никнэйм', max_length=50, blank=True)
    birth_date = models.DateField('Дата рождения', null=True, blank=False)
    city = models.CharField('Город', max_length=50, blank=False)
    photo = models.ImageField('Фотография для бейджа', upload_to='badge_photo/', blank=True)
    children_info = models.TextField('Информация о детях', blank=True)
    charge_person = models.ForeignKey(to='User', verbose_name='Информация об ответственном лице',
                                      blank=True, null=True, on_delete=models.SET_NULL)
    addition_info = models.TextField('Дополнительная информация (для модераторов)', blank=True)
    discount = models.IntegerField('Скидка', default=0, help_text='Указывается в рублях')

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password',]

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['last_name']

    def __str__(self):
        return f"{self.first_name} {self.last_name}" if self.last_name else self.email

    def get_absolute_url(self):
        return reverse('user_profile')

    def get_order(self):
        has_group = self.has_group()

        if len(self.order_set.all()) > 0:
            order = self.order_set.all()[0]
        elif has_group and len(has_group.placement_group.autor.order_set.all()) > 0:
            order = self.has_group().placement_group.autor.order_set.all()[0]
        else:
            return None

        if order.order_type == 'day_p':
            name = order.get_penetration_type().title
        else:
            name = f"{order.room.block.name}, {order.room.room_number}" if order.room else f"{order}"

        price_by_people = [{
            'name': u,
            'place': u.get_human_place_type(),
            'price': u.get_price()
        } for u in (order.userorder_set.all() \
                    if order.room and order.room.is_luxary \
                    else order.userorder_set.filter(user=self))]

        price_by_people.extend([{
            'name': ch,
            'place': ch.get_human_place_type(),
            'price': ch.get_price()
        } for ch in (order.childrenorder_set.all() \
                     if order.room and order.room.is_luxary \
                     else order.childrenorder_set.filter(children__user=self))])

        return {
            'name': name,
            'total_price': order.get_base_price(),
            'price_by_people': price_by_people,
            'order': order
        }

    def get_eat_amount(self):
        user_eat_orders = self.eatorder_set.all()
        return reduce(
            lambda x, y: (x.eat_order_price() if hasattr(x, 'eat_order_price') else x) + y.eat_order_price(),
            user_eat_orders, 0
        ) if user_eat_orders else 0

    def get_room_amount(self, total=False):
        try:
            order = self.get_order()['order']
        except (IndexError, TypeError):
            return 0

        # Если чувак владелец группы и снял люкс, то возвращаем полную стоимость заказа
        if self.has_group_owner() and order.room.is_luxary or total:
            return order.get_base_price() - self.discount

        # Если чувак состоит в группе, то все сильно уложняется
        if self.has_group():
            # Если это люкс, то оплата ложится на автора бронирования
            if order.room.is_luxary:
                return 0

            my_order = list(
                filter(
                    lambda u: u.user.pk is self.pk,
                    order.userorder_set.all()
                )
            )

            if not my_order:
                return 0

            price = my_order[0].get_price()

            for childorder in order.childrenorder_set.all():
                if childorder.children.user.pk is self.pk:
                    price += childorder.get_price()

            if order.without_other_mat_people and order.user.pk is self.pk:
                price += global_preferences['prices__price_for_alone_group']

            return price - self.discount

        return order.get_base_price() - self.discount

    def get_total_amount(self):
        return self.get_eat_amount() + self.get_room_amount()

    def has_group(self):
        if len(self.membership_set.all()) > 0:
            return self.membership_set.all()[0]
        else:
            return None

    def has_group_owner(self):
        if self.placementgroup_set.all():
            return self.placementgroup_set.all()[0]
        else:
            return None


class Children(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    age = models.IntegerField('Возраст (полных лет)', validators=[
        MaxValueValidator(17),
        MinValueValidator(0)
    ])
    name = models.CharField('ФИО ребенка', max_length=255, default='')

    class Meta:
        verbose_name = 'Ребенок'
        verbose_name_plural = 'Дети'

    def __str__(self):
        return f"{self.name} ({self.age} лет)"

    def get_discount(self):
        if self.age <= 7:
            return global_preferences['children_contribution__children_up_to_7_alone']

        if self.age > 7 and self.age <= 12:
            return global_preferences['children_contribution__children_7_12']

        if self.age > 12 and self.age <= 16:
            return global_preferences['children_contribution__children_13_16']

        return 0
