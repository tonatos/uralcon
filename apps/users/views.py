from django.http import JsonResponse
from django.contrib.auth.views import LoginView
from django.views.generic import UpdateView, CreateView, View, DeleteView
from django.views.generic.base import TemplateResponseMixin
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import PasswordResetView

from .forms import UserRegistrationForm, UserProfileForm
from .models import User, Children


class UsersLoginView(LoginView):
    template_name = 'users/login.html'

    def get(self, *args):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect('/users/profile')
        else:
            return super(UsersLoginView, self).get(args)


class UserRegistrationView(CreateView):
    form_class = UserRegistrationForm
    success_url = reverse_lazy('login')
    template_name = 'users/registration.html'

    def get(self, *args):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect('/users/profile')
        else:
            return super(UserRegistrationView, self).get(args)

    def form_valid(self, form):
        password = self.request.POST.get('password1')

        user = form.save(False)
        user.set_password(password)
        user.save()

        user = authenticate(username=user.email, password=password)
        login(self.request, user)

        return super(UserRegistrationView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class ProfileView(UpdateView):
    form_class = UserProfileForm
    template_name = 'users/profile.html'
    model = User

    def get_object(self):
        return self.request.user

    def form_valid(self, form):
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class ChildrenView(CreateView):
    model = Children
    fields = ['name', 'age']
    template_name = 'users/children.html'
    success_url = reverse_lazy('user_children')

    def form_valid(self, form):
        children = form.save(False)
        children.user = self.request.user
        children.save()

        return super(ChildrenView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class ChildrenDeleteView(DeleteView):
    model = Children
    success_url = reverse_lazy('user_children')

    def delete(self, request, *args, **kwargs):
        try:
            children = Children.objects.get(pk=kwargs['pk'], user=request.user)
            return super().delete(request, *args, **kwargs)
        except:
            raise Http404


@method_decorator(login_required, name='dispatch')
class ChildrenListView(View, TemplateResponseMixin):
    http_method_names = ['get']

    def get(self, request):
        return JsonResponse({
            'children': [{
                'age': i.age,
                'name': i.name
            } for i in Children.objects.filter(user=request.user)]
        }, safe=True, content_type="application/json")


@method_decorator(login_required, name='dispatch')
class GetRoomAmountView(View, TemplateResponseMixin):
    http_method_names = ['get']

    def get(self, request):
        return JsonResponse({
            'children': request.user.get_room_amount()
        }, safe=True, content_type="application/json")
