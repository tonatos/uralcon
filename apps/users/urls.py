from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('login/', views.UsersLoginView.as_view(), name='login'),
    path('logout/', login_required(auth_views.LogoutView.as_view(
        template_name='users/logout.html')), name='logout'),
    path('password-change/', login_required(auth_views.PasswordChangeView.as_view()), name='password_change'),
    path('password-change-done/', login_required(auth_views.PasswordChangeDoneView.as_view())),
    path('password-reset/', auth_views.PasswordResetView.as_view(
            template_name='users/password_reset_form.html',
            email_template_name='users/password_reset_email.html'
        ), name='password_reset'),
    path('password-reset-done/', auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('password-reset-complite/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('registration/', views.UserRegistrationView.as_view(), name='registration'),
    path('profile/', login_required(views.ProfileView.as_view()), name='user_profile'),
    path('children/', login_required(views.ChildrenView.as_view()), name='user_children'),
    path('children/delete/<int:pk>/', login_required(views.ChildrenDeleteView.as_view()), name='user_children_delete'),

    path('children-list/', views.ChildrenListView.as_view()),

    path('rule/', TemplateView.as_view(template_name='users/rule.html'), name="rule"),
    path('personal-data-processing/', TemplateView.as_view(template_name='users/personal-data-processing.html'),
         name="personal_data_processing")
]
