# Generated by Django 2.1.4 on 2019-01-10 23:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Block',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название корпуса')),
                ('schema', models.ImageField(null=True, upload_to='schemas/', verbose_name='Схемам корпуса')),
            ],
            options={
                'verbose_name': 'Корпус',
                'verbose_name_plural': 'Корпуса',
            },
        ),
        migrations.CreateModel(
            name='Bus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.TimeField(verbose_name='Время')),
                ('places', models.IntegerField(default=14, verbose_name='Количество мест')),
                ('direction', models.CharField(choices=[('from_city', 'Екатеринбург – Хрустальная'), ('to_city', 'Хрустальная – Екатеринбург')], default='from_city', max_length=10, verbose_name='Направление')),
            ],
            options={
                'verbose_name': 'Автобус',
                'verbose_name_plural': 'Автобусы',
                'ordering': ['time'],
            },
        ),
        migrations.CreateModel(
            name='BusOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
            ],
            options={
                'verbose_name': 'Заказ на автобус',
                'verbose_name_plural': 'Заказы на автобус',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='EatOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('eat_type', models.CharField(choices=[('fr_lunch', 'Пятница обед'), ('fr_dinner', 'Пятница ужин'), ('st_lunch', 'Суббота обед'), ('st_dinner', 'Суббота ужин'), ('sn_lunch', 'Воскресенье обед')], default=None, max_length=10, verbose_name='Питание')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
            ],
            options={
                'verbose_name': 'Заказ на еду',
                'verbose_name_plural': 'Заказы на еду',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='EventRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='ФИО (+ никнейм) лица, ответственного за мероприятие. \n                     Контактный номер телефона и адрес электронной почты', max_length=255, verbose_name='ФИО')),
                ('description', models.TextField(help_text='Описание сути мероприятия для организаторов фестиваля.', verbose_name='Описание')),
                ('public_description', models.TextField(help_text='Описание мероприятия для сайта и паблика: ярко, красиво, \n                   внятно. Помните: привлекательный анонс - половина успеха!', verbose_name='Описание для сайта')),
                ('what_need', models.TextField(help_text='Что требуется от организаторов конвента (оборудование, \n                     призы и др.)?', verbose_name='Что требуется')),
                ('what_has', models.TextField(help_text='Что уже есть и/или будет обеспечено силами организаторов \n                     мероприятия (оборудование, призы, оформление, реклама \n                     и др). ', verbose_name='Что уже есть')),
                ('placement', models.TextField(help_text='Какое помещение понадобится для проведения мероприятия?', verbose_name='Помещение')),
                ('time', models.TextField(help_text='В какое время вы хотели бы провести мероприятие (число, \n                     время, продолжительность)? ', verbose_name='Время проведения')),
                ('need_charter', models.TextField(help_text='Хотели бы вы получить почетную грамоту с символикой \n                     фестиваля: да / нет. Если «да», то:<br>\n                     — количество грамот<br>\n                     — для каждой грамоты: полное наименование организации (если есть), \n                     должность (если есть), Фамилия Имя', verbose_name='Грамота')),
                ('is_accepted', models.BooleanField(default=False, verbose_name='Заявка принята')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
            ],
            options={
                'verbose_name': 'Заявка на мероприятие',
                'verbose_name_plural': 'Заявки на мероприятия',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Membership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_type', models.CharField(choices=[('room', 'Заселение'), ('day_p', 'Дневная проходка')], default='room', max_length=5, verbose_name='Тип брони')),
                ('place_type', models.CharField(blank=True, choices=[('bed', 'Кровать'), ('mat', 'Пенка')], default='bed', max_length=5, verbose_name='Тип места')),
                ('day_penetration_type', models.CharField(choices=[('all', 'На все дни'), ('one', 'Только суббота'), ('st_n', 'Ночь субботы')], default='all', max_length=5, verbose_name='Тип проходки')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='Комментарий о пользователе')),
                ('is_payed', models.BooleanField(default=False, verbose_name='Оплачена')),
                ('accepted', models.BooleanField(default=False, help_text='При выставлении галочки пользователю приходит уведомление \n                     о том, что его бронь принята', verbose_name='Подтверждена')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='PlacementGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название группы')),
                ('size', models.IntegerField(default=4, verbose_name='Размер группы')),
                ('password', models.CharField(help_text='Пароль нужен для того, чтобы другие участники \n                     могли безопасно присоединяться к вашей группе', max_length=100, verbose_name='Пароль')),
            ],
            options={
                'verbose_name': 'Группа заселения',
                'verbose_name_plural': 'Группы заселения',
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('room_number', models.CharField(max_length=5, verbose_name='Номер комнаты')),
                ('is_luxary', models.BooleanField(default=False, verbose_name='Номер люкс')),
                ('bed_count', models.IntegerField(default=0, verbose_name='Количество кроватей')),
                ('mat_count', models.IntegerField(default=0, verbose_name='Количество пенок')),
                ('price_on_bed', models.IntegerField(default=0, verbose_name='Цена за место на кровате')),
                ('price_on_mat', models.IntegerField(default=0, verbose_name='Цена за место на пенке')),
                ('block', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='order.Block', verbose_name='Корпус')),
            ],
            options={
                'verbose_name': 'Комната',
                'verbose_name_plural': 'Комнаты',
            },
        ),
    ]
