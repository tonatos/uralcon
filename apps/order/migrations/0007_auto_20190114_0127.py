# Generated by Django 2.1.4 on 2019-01-14 01:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('order', '0006_order_without_other_mat_people'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('place_type', models.CharField(blank=True, choices=[('bed', 'Кровать'), ('mat', 'Пенка')], default='bed', max_length=5, null=True, verbose_name='Тип места')),
            ],
            options={
                'verbose_name': 'Место для взрослого',
                'verbose_name_plural': 'Места для взрослых',
            },
        ),
        migrations.RemoveField(
            model_name='order',
            name='place_type',
        ),
        migrations.AddField(
            model_name='userorder',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='order.Order', verbose_name='Заказ'),
        ),
        migrations.AddField(
            model_name='userorder',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
    ]
