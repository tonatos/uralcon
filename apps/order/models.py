from functools import reduce

from django.db import models
from django.db.models.signals import post_save, post_init
from django.urls import reverse
from django.core.mail import send_mail
from django.conf import settings
from django.utils.functional import cached_property

from dynamic_preferences.registries import global_preferences_registry

from apps.users.models import User, Children


global_preferences = global_preferences_registry.manager()


def get_list_value(value, list):
    try:
        return [dir[1] for dir in list if dir[0] == value][0]
    except IndexError:
        return False


class Block(models.Model):
    name = models.CharField('Название корпуса', max_length=100)
    schema = models.ImageField('Схемам корпуса', upload_to='schemas/', null=True)

    class Meta:
        verbose_name = 'Корпус'
        verbose_name_plural = 'Корпуса'

    def __str__(self):
        return f"{self.name}"

    def get_free_bed(self):
        free = 0
        for r in self.room_set.all():
            free += r.get_free_bed()
        return free

    def get_free_mat(self):
        free = 0
        for r in self.room_set.all():
            free += r.get_free_mat()
        return free


class Room(models.Model):
    room_number = models.CharField('Номер комнаты', max_length=5)
    is_luxary = models.BooleanField('Номер люкс', default=False)
    bed_count = models.IntegerField('Количество кроватей', default=0)
    mat_count = models.IntegerField('Количество пенок', default=0)
    price_on_bed = models.IntegerField('Цена за место на кровати', default=0)
    price_on_mat = models.IntegerField('Цена за место на пенке', default=0)
    block = models.ForeignKey(
        Block, verbose_name='Корпус',
        on_delete=models.SET_NULL, null=True
    )

    class Meta:
        verbose_name = 'Комната'
        verbose_name_plural = 'Комнаты'

    def __str__(self):
        return f"{self.block.name}: {self.room_number}" if self.block else self.room_number

    def human_in_room(self):
        return self.bed_count - self.get_free_bed()

    def get_free_bed(self):
        block_place = 0
        orders = self.order_set.all()
        for o in orders:
            for child in o.childrenorder_set.all():
                if child.place_type == 'bed':
                    block_place += 1

            for user in o.userorder_set.all():
                if user.place_type == 'bed':
                    block_place += 1

        return self.bed_count - block_place

    def get_free_mat(self):
        block_place = 0
        orders = self.order_set.all()
        for o in orders:
            for child in o.childrenorder_set.all():
                if child.place_type == 'mat':
                    block_place += 1

            for user in o.userorder_set.all():
                if user.place_type == 'mat':
                    block_place += 1

        return self.mat_count - block_place


class PlacementGroup(models.Model):
    autor = models.ForeignKey(
        User, verbose_name='Автор группы',
        on_delete=models.CASCADE, default=None, null=True)

    users = models.ManyToManyField(
        User,
        through='Membership',
        through_fields=('placement_group', 'user'),
        related_name='placement_users'
    )

    name = models.CharField('Название группы', max_length=100)
    size = models.IntegerField('Размер группы', default=4, choices=[[i,i] for i in range(2,8)])
    password = models.CharField(
        'Пароль', 
        max_length=100, 
        help_text="""Пароль нужен для того, чтобы другие участники 
                     могли безопасно присоединяться к вашей группе"""
     )

    class Meta:
        verbose_name = 'Группа заселения'
        verbose_name_plural = 'Группы заселения'
        ordering = ['name']

    def __str__(self):
        return f"{self.name} (размер: {self.size})"

    def get_absolute_url(self):
        return reverse('order')

    def get_all_participant(self):
        participant = list(self.users.all())
        for u in self.users.all():
            participant.extend(list(u.children_set.all()))
        return participant


class Membership(models.Model):
    placement_group = models.ForeignKey(
        PlacementGroup, 
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
        

class Bus(models.Model):
    DIRECTION = (
        ('from_city', 'Екатеринбург – Хрустальная'),
        ('to_city', 'Хрустальная – Екатеринбург'),
    )
    time = models.TimeField('Время', blank=False)
    places = models.IntegerField('Количество мест', default=14)
    direction = models.CharField(
        'Направление', choices=DIRECTION, max_length=10, default='from_city')

    class Meta:
        verbose_name = 'Автобус'
        verbose_name_plural = 'Автобусы'
        ordering = ['time']

    def __str__(self):
        return f"{self.get_direction()}: {self.time}" or f"{self.time}"

    def get_direction(self):
        return get_list_value(self.direction, self.DIRECTION)


class EventRequest(models.Model):
    name = models.CharField(
        'ФИО ответственного лица',
        max_length=255, 
        help_text="""ФИО (+ никнейм) лица, ответственного за мероприятие. 
                     Контактный номер телефона и адрес электронной почты""")

    contact_info = models.TextField(
        'Контактная информация', max_length=300,
        help_text="""Телефон, ID Вконтакте, E-mail""")

    description = models.TextField(
        'Описание', 
        help_text="""Описание сути мероприятия для организаторов фестиваля.""")

    public_description = models.TextField(
        'Описание для сайта', 
        help_text="""Описание мероприятия для сайта и паблика: ярко, красиво, 
                   внятно. Помните: привлекательный анонс - половина успеха!""")

    what_need = models.TextField(
        'Что требуется', 
        help_text="""Что требуется от организаторов конвента (оборудование, 
                     призы и др.)?""")

    what_has = models.TextField(
        'Что уже есть', 
        help_text="""Что уже есть и/или будет обеспечено силами организаторов 
                     мероприятия (оборудование, призы, оформление, реклама 
                     и др). """)

    placement = models.TextField(
        'Помещение', 
        help_text="""Какое помещение понадобится для проведения мероприятия?""")

    time = models.TextField(
        'Время проведения', 
        help_text="""В какое время вы хотели бы провести мероприятие (число, 
                     время, продолжительность)? """)

    need_charter = models.TextField(
        'Грамота', 
        help_text="""Хотели бы вы получить почетную грамоту с символикой 
                     фестиваля: да / нет. Если «да», то:<br>
                     — количество грамот<br>
                     — для каждой грамоты: полное наименование организации (если есть), 
                     должность (если есть), Фамилия Имя""")

    user = models.ForeignKey(
        User, verbose_name='Пользователь',
        on_delete=models.CASCADE)

    is_accepted = models.BooleanField(
        'Заявка принята', default=False)

    created_at = models.DateTimeField(
        'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Заявка на мероприятие'
        verbose_name_plural = 'Заявки на мероприятия'
        ordering = ['-created_at']

    def get_absolute_url(self):
        return reverse('event_request')

    def __str__(self):
        return f"{self.name}, от {self.created_at.strftime('%d.%m.%y, в %H:%M')}"


class EatOrder(models.Model):
    EAT_TYPE = (
        ('fr_lunch', 'Пятница обед'),
        ('fr_dinner', 'Пятница ужин'),
        ('st_lunch', 'Суббота обед'),
        ('st_dinner', 'Суббота ужин'),
        ('sn_lunch', 'Воскресенье обед'),
    )

    EAT_TYPE_MAP_PRICE = (
        ('fr_lunch', 'eat_lunch'),
        ('fr_dinner', 'eat_dinner'),
        ('st_lunch', 'eat_lunch'),
        ('st_dinner', 'eat_dinner'),
        ('sn_lunch', 'eat_lunch'),
    )

    eat_type = models.CharField(
        'Питание',
        max_length=10,
        choices=EAT_TYPE,
        default=None
    )

    user = models.ForeignKey(
        User, verbose_name='Пользователь',
        on_delete=models.CASCADE)

    created_at = models.DateTimeField(
        'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Заказ на еду'
        verbose_name_plural = 'Заказы на еду'
        ordering = ['-created_at']

    def __str__(self):
        return f"{self.user} ({get_list_value(self.eat_type, self.EAT_TYPE)}), " \
               f"от {self.created_at.strftime('%d.%m.%y, в %H:%M')}"

    @staticmethod
    def get_price_by_key(key):
        return global_preferences[f'eat__{get_list_value(key, EatOrder.EAT_TYPE_MAP_PRICE)}']

    @staticmethod
    def get_eat_type():
        return [
            {
                'title': i[1],
                'name': i[0],
                'price': EatOrder.get_price_by_key(i[0])
            } for i in EatOrder.EAT_TYPE
        ]

    def eat_order_price(self):
        return EatOrder.get_price_by_key(self.eat_type)


class BusOrder(models.Model):
    bus = models.ForeignKey(
        Bus, on_delete=models.SET_NULL,
        null=True)

    user = models.ForeignKey(
        User, verbose_name='Пользователь',
        on_delete=models.CASCADE)

    created_at = models.DateTimeField(
        'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Заказ на автобус'
        verbose_name_plural = 'Заказы на автобус'
        ordering = ['-created_at']

    def __str__(self):
        return f"{self.user} ({self.bus.time}, {self.bus.get_direction()}), " \
               f"от {self.created_at.strftime('%d.%m.%y, в %H:%M')}" if self.bus else f"{self.user} (без автобуса)"


class Order(models.Model):
    ORDER_TYPE = (
        ('room', 'Заселение'),
        ('day_p', 'Дневная проходка'),
    )

    PENETRATION_TYPE = (
        ('all', 'Дневная проходка «На все дни»'),
        ('one', 'Дневная проходка «Только суббота»'),
        ('st_n', 'Ночная проходка «Ночь субботы»'),
    )

    PENETRATION_TYPE_MAP_PRICE = (
        ('all', 'add_day_ticket'),
        ('one', 'only_saturday_ticket'),
        ('st_n', 'only_saturday_night_ticket'),
    )

    user = models.ForeignKey(
        User, verbose_name='Пользователь',
        on_delete=models.CASCADE
    )

    room = models.ForeignKey(
        Room,
        verbose_name='Комната',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    placement_group = models.ForeignKey(
        PlacementGroup,
        verbose_name='Группа поселения',
        default=None,
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    order_type = models.CharField(
        'Тип брони',
        max_length=5,
        choices=ORDER_TYPE,
        default='room'
    )
    day_penetration_type = models.CharField(
        'Тип проходки',
        choices=PENETRATION_TYPE,
        max_length=5,
        default=None,
        null=True,
        blank=True,
    )
    without_other_mat_people = models.BooleanField(
        'Доплата за отсутствие пеночников',
        default=False,
    )

    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    comment = models.TextField('Комментарий о пользователе', blank=True, null=True)
    is_payed = models.BooleanField('Оплачена', default=False)
    accepted = models.BooleanField(
        'Подтверждена',
        default=False,
        help_text="""При выставлении галочки пользователю приходит уведомление 
                     о том, что его бронь принята"""
    )

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        try:
            return f"{self.user}, в {self.created_at.strftime('%d.%m.%y, в %H:%M')}"
        except:
            return self.pk

    @staticmethod
    def get_price_by_key(key):
        return global_preferences[f'prices__{get_list_value(key, Order.PENETRATION_TYPE_MAP_PRICE)}']

    @staticmethod
    def get_penetration_types():
        return [
            {
                'title': i[1],
                'name': i[0],
                'price': Order.get_price_by_key(i[0])
            } for i in Order.PENETRATION_TYPE
        ]

    def get_penetration_type(self):
        return get_list_value(self.day_penetration_type, Order.PENETRATION_TYPE)

    def free_beds_in_order(self):
        total_guests = len(self.userorder_set.filter(place_type='bed')) + len(self.childrenorder_set.filter(place_type='bed'))
        if total_guests < self.room.bed_count:
            return [{
                'price': self.room.price_on_bed
            } for i in range(0, (self.room.bed_count - total_guests))]
        else:
            return False

    def get_base_price(self):
        if self.order_type == 'day_p':
            price = self.get_price_by_key(self.day_penetration_type)
            total_price = price

            for ch in self.user.children_set.all():
                total_price += price - ch.get_discount()
        else:
            total_price = 0

            for user in self.userorder_set.all():
                total_price += user.get_price()

            for user in self.childrenorder_set.all():
                total_price += user.get_price()

            if self.without_other_mat_people:
                total_price += global_preferences['prices__price_for_alone_group']

            # смотрим, если не все места люкса заняты, но в группе посления они заявлен, то все равно плюсуем для оплаты
            if self.room and self.room.is_luxary and self.free_beds_in_order() is not False:
                total_price += reduce(lambda acc, room: acc + room['price'], self.free_beds_in_order(), 0)

        return total_price

    @staticmethod
    def send_email(instance, message):
        send_mail(
            'Изменился статус вашего бронирования на фестивале «Уралкон»',
            message,
            settings.DEFAULT_FROM_EMAIL,
            [instance.user.email],
            fail_silently=False,
        )

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get('instance')
        if instance.previous_accepted != instance.accepted:
            Order.send_email(instance, 'Статус вашего бронирования измененен на «%s»' % (
                'Подтверждено' if instance.accepted else 'Отменено'
            ))

        if instance.previous_is_payed != instance.is_payed:
            Order.send_email(instance, 'Статус оплаты вашего бронирования измененен на «%s»' % (
                'Оплачено' if instance.is_payed else 'Не оплачено'
            ))

    @staticmethod
    def remember_state(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.previous_is_payed = instance.is_payed
        instance.previous_accepted = instance.accepted


class UserOrder(models.Model):
    USER_PLACE_TYPE = (
        ('bed', 'Кровать'),
        ('mat', 'Пенка'),
    )

    order = models.ForeignKey(
        Order, verbose_name='Заказ',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User, verbose_name='Пользователь',
        on_delete=models.CASCADE
    )
    place_type = models.CharField(
        'Тип места',
        max_length=5,
        choices=USER_PLACE_TYPE,
        default='bed',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Место для взрослого'
        verbose_name_plural = 'Места для взрослых'

    def __str__(self):
        return f"{self.user}"

    def get_human_place_type(self):
        return get_list_value(self.place_type, UserOrder.USER_PLACE_TYPE)

    def get_price(self):
        if self.order.room:
            if self.place_type == 'mat':
                return self.order.room.price_on_mat

            if self.place_type == 'bed':
                return self.order.room.price_on_bed

        if not self.place_type:
            return self.order.get_base_price()

        return 0


class ChildrenOrder(models.Model):
    CHILDREN_PLACE_TYPE = (
        ('together', 'Спит с родителем'),
        ('bed', 'Кровать'),
        ('mat', 'Пенка'),
    )

    order = models.ForeignKey(
        Order, verbose_name='Заказ',
        on_delete=models.CASCADE
    )
    children = models.ForeignKey(
        Children, verbose_name='Ребенок',
        on_delete=models.CASCADE
    )
    place_type = models.CharField(
        'Тип места',
        max_length=10,
        choices=CHILDREN_PLACE_TYPE,
        default='bed',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Место для ребенка'
        verbose_name_plural = 'Места для детей'

    def __str__(self):
        return f"{self.children.user}, {self.children}"

    def get_human_place_type(self):
        return get_list_value(self.place_type, ChildrenOrder.CHILDREN_PLACE_TYPE)

    def get_price(self):
        if self.order.room:
            if self.place_type == 'mat':
                return self.order.room.price_on_mat - self.children.get_discount()

            if self.place_type == 'bed':
                return self.order.room.price_on_bed - self.children.get_discount()

        if self.place_type == 'together':
            return 0

        if not self.place_type:
            return self.order.get_base_price() - self.children.get_discount()

        return 0

post_save.connect(Order.post_save, sender=Order)
post_init.connect(Order.remember_state, sender=Order)
