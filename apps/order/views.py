import datetime
import json

from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic.list import ListView
from django.db.models import F, Count, Subquery, OuterRef
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.http import Http404, HttpResponseRedirect
from django.db.models import Q

from dynamic_preferences.registries import global_preferences_registry

from .forms import EventRequestForm
from .models import (
    EventRequest, Bus, BusOrder, EatOrder,
    Block, Room, PlacementGroup, Order,
    ChildrenOrder, UserOrder, Membership
)


global_preferences = global_preferences_registry.manager()


def _clear_order_item(users_pk=[], childrens_pk=[]):
    if users_pk:
        UserOrder.objects.filter(user__pk__in=users_pk).delete()

    if childrens_pk:
        ChildrenOrder.objects.filter(children__pk__in=childrens_pk).delete()


class OrderLimitMixin(object):
    order_stop = False

    def get_context_data(self, **kwargs):
        date_from = global_preferences['registration__registration_start']
        date_to = global_preferences['registration__registration_stop']
        now = datetime.datetime.now().date()

        if now < date_from or now > date_to:
            self.order_stop = True

        context = {'order_stop': self.order_stop}
        context.update(super().get_context_data(**kwargs))
        return context

    def dispatch(self, request, *args, **kwargs):
        if self.request.method == 'POST' and self.order_stop:
            raise Http404
        else:
            return super().dispatch(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class OrderView(OrderLimitMixin, TemplateView):
    template_name = 'order/order_order.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        can_order = True

        if len(self.request.user.order_set.all()) > 0:
            can_order = False

        if self.request.user.has_group() and not self.request.user.has_group_owner():
            can_order = False

        context.update({
            'can_order': can_order,
            'without_other_mat_people': global_preferences['prices__price_for_alone_group'],
            'discount': {
                'children_up_to_7_alone': global_preferences['children_contribution__children_up_to_7_alone'],
                'children_7_12': global_preferences['children_contribution__children_7_12'],
                'children_13_16': global_preferences['children_contribution__children_13_16'],
            },
        })
        return context

    def post(self, request, **kwargs):
        result = {'status': True}

        if len(request.user.order_set.all()) > 0 and not self.instance:
            return JsonResponse(
                {
                    'status': False,
                    'message': 'У вас уже есть действующий брони'
                },
                safe=True, content_type="text/json", status=200)

        users = []
        childrens = []
        without_other_mat_people = request.POST.get('without_other_mat_people', False)

        room = get_object_or_404(Room, pk=request.POST.get('room', None))

        if request.POST.get('placement_group', None):
            placement_group = get_object_or_404(PlacementGroup, pk=request.POST.get('placement_group'))
        else:
            placement_group = None

        order = Order(
            user=request.user,
            order_type='room',
            room=room,
            placement_group=placement_group,
        ) if not hasattr(self, 'instance') else self.instance

        order.without_other_mat_people=without_other_mat_people

        for k, item in request.POST.items():
            if 'children-' in k:
                childrens.append({
                    'order': order,
                    'children_id': int(k.replace('children-', '')),
                    'place_type': item
                })

            if 'user-' in k:
                users.append({
                    'order': order,
                    'user_id': int(k.replace('user-', '')),
                    'place_type': item
                })

        bed_guests = len(list(filter(lambda d: d['place_type'] in ['bed'], childrens + users)))
        mat_guests = len(list(filter(lambda d: d['place_type'] in ['mat'], childrens + users)))
        together_guests = len(list(filter(lambda d: d['place_type'] in ['together'], childrens + users)))

        if bed_guests > room.bed_count:
            result = {
                'status': False,
                'message': 'Количества кроватей в номере не хватает для всех гостей, которых вы расположили в кроватях'
            }

        if mat_guests > room.mat_count:
            result = {
                'status': False,
                'message': 'Количества пенок в номере не хватает для всех гостей, которых вы расположили в пенках'
            }

        if request.user.has_group_owner():
            if len(request.user.has_group_owner().get_all_participant()) - together_guests > mat_guests + bed_guests:
                result = {
                    'status': False,
                    'message': 'Вы разместили не всех гостей'
                }
        else:
            if len(request.user.children_set.all()) - together_guests + 1 > mat_guests + bed_guests:
                result = {
                    'status': False,
                    'message': 'Вы разместили не всех гостей'
                }

        # if room.is_luxary and placement_group.size != room.bed_count:
        #     result = {
        #         'status': False,
        #         'message': """Люкс можно забронировать только целиком. Все кровати должны быть заняты и размер вашей
        #                       группы не должен превышать количество кроватей. В номере %s кроватей, в вашей группе
        #                       %s человек""" % (room.bed_count, placement_group.size)
        #     }

        if result['status']:
            _clear_order_item([u['user_id'] for u in users], [ch['children_id'] for ch in childrens])

            order.save()

            for u in users:
                user = UserOrder(**u)
                user.save()

            for ch in childrens:
                children = ChildrenOrder(**ch)
                children.save()

        return JsonResponse(
            result,
            safe=True, content_type="text/json", status=200)


@method_decorator(login_required, name='dispatch')
class OrderEditView(OrderView):
    template_name = 'order/order_order_edit.html'

    def dispatch(self, request, *args, **kwargs):
        self.instance = get_object_or_404(Order, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not hasattr(self.instance, 'room') or not self.instance.room:
            raise Http404

        context.update({
            'instance': self.instance,
            'selectedRoom': json.dumps({
                'is_luxary': self.instance.room.is_luxary,
                'bed_count': self.instance.room.bed_count,
                'mat_count': self.instance.room.mat_count,
                'price_on_bed': self.instance.room.price_on_bed,
                'price_on_mat': self.instance.room.price_on_mat,
                'free_bed': self.instance.room.get_free_bed(),
                'free_mat': self.instance.room.get_free_mat(),
            }),
        })
        return context


@method_decorator(login_required, name='dispatch')
class OrderDeleteView(OrderLimitMixin, DeleteView):
    model = Order
    success_url = reverse_lazy('order')

    def delete(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(pk=kwargs['pk'], user=request.user)
            ChildrenOrder.objects.filter(order=order).delete()
            UserOrder.objects.filter(order=order).delete()
            return super().delete(request, *args, **kwargs)
        except:
            raise Http404


@method_decorator(login_required, name='dispatch')
class OrderConnectGroupView(OrderLimitMixin, TemplateView):
    template_name = 'order/order_connect_group.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'groups': PlacementGroup.objects\
                                    .annotate(user_count=Count('users'))\
                                    .annotate(orders=Count('order'))\
                                    .filter(user_count__lte=F('size'))\
                                    #.filter(orders=0)
        })
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        placement_group = request.POST.get('placement_group', None)
        password = request.POST.get('password', None)

        if placement_group and password:
            placement_group = get_object_or_404(PlacementGroup, pk=placement_group)

            if len(Membership.objects.filter(user=request.user)) > 0:
                context.update({
                    'message': {
                        'message': f"Вы уже являетесь участников одной из групп",
                        'status': False
                    }})
                return self.render_to_response(context)

            if placement_group.size < (
                    len(placement_group.get_all_participant()) + \
                    len(request.user.children_set.all()) + 1):
                context.update({
                    'message': {
                        'message': f"В группе не хватает мест, чтобы добавить вас и ваших детей",
                        'status': False
                    }})
                return self.render_to_response(context)

            if placement_group.password == password:
                # Если у нас дурацкий люкс, оформленный на группу, но в нем не заняты все места,
                # и мы вот присоединяемся к группе, то надо добавиться в качестве UserOrder
                try:
                    placement_order = placement_group.order_set.all()[0]
                    if placement_order.room.is_luxary and placement_order.free_beds_in_order() is not False:
                        user_o = UserOrder(**{
                            'order': placement_order,
                            'user': request.user
                        })
                        user_o.save()

                        for ch in request.user.children_set.all():
                            children_o = ChildrenOrder(**{
                                'order': placement_order,
                                'children': ch
                            })
                            children_o.save()
                except IndexError:
                    pass

                context.update({
                    'message': {
                        'message': f"Вы успешно вступили в группу {placement_group.name}!",
                        'status': True
                    }})
                users = Membership(
                    placement_group=placement_group,
                    user=self.request.user
                )
                users.save()
                return HttpResponseRedirect('/order/order/alone')

            else:
                context.update({
                    'message': {
                        'message': 'Вы ошиблись паролем. Укажите правильный пароль',
                        'status': False
                    }})

        return self.render_to_response(context)


@method_decorator(login_required, name='dispatch')
class OrderCreateGroupView(OrderLimitMixin, CreateView):
    model = PlacementGroup
    success_url = reverse_lazy('order')
    template_name = 'order/order_create_group.html'
    fields = ['name', 'password', 'size']

    def form_valid(self, form):
        group = form.save(False)
        group.autor = self.request.user
        group.save()

        users = Membership(
            placement_group=group,
            user=self.request.user
        )
        users.save()

        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class OrderEditGroupView(OrderLimitMixin, UpdateView):
    model = PlacementGroup
    template_name = 'order/order_create_group.html'
    fields = ['name', 'password', 'size']

    def get_object(self, *args, **kwargs):
        return PlacementGroup.objects.get(pk=self.kwargs['pk'])



@method_decorator(login_required, name='dispatch')
class OrderDeleteGroupView(OrderLimitMixin, DeleteView):
    model = PlacementGroup
    success_url = reverse_lazy('order')

    def delete(self, request, *args, **kwargs):
        try:
            group = PlacementGroup.objects.get(pk=kwargs['pk'], autor=request.user)
            Membership.objects.filter(placement_group=group).delete()
            return super().delete(request, *args, **kwargs)
        except:
            raise Http404


@method_decorator(login_required, name='dispatch')
class OrderLeaveGroupView(OrderLimitMixin, DeleteView):
    model = Membership
    success_url = reverse_lazy('order')

    def delete(self, request, *args, **kwargs):
        try:
            membership = Membership.objects.get(pk=kwargs['pk'])
            _clear_order_item([membership.user.pk], membership.user.children_set.all().values('pk'))
            return super().delete(request, *args, **kwargs)
        except:
            raise Http404

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class OrderPerDayView(OrderLimitMixin, TemplateView):
    template_name = 'order/order_per_day.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'penetration_type': Order.get_penetration_types()
        })
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        if request.user.get_order():
            return self.render_to_response(context)

        if request.POST.get('PENETRATION_TYPE', False):
            _clear_order_item([request.user.pk], [ch.pk for ch in request.user.children_set.all()])

            order = Order(
                user=request.user,
                room=None,
                placement_group=None,
                order_type='day_p',
                day_penetration_type=request.POST.get('PENETRATION_TYPE')
            )
            order.save()

            userorder = UserOrder(
                user=request.user,
                order=order,
                place_type=None
            )
            userorder.save()

            for children in request.user.children_set.all():
                ch_order = ChildrenOrder(
                    children=children,
                    order=order,
                    place_type=None
                )
                ch_order.save()
            return HttpResponseRedirect(reverse_lazy('order'))

        return self.render_to_response(context)


@method_decorator(login_required, name='dispatch')
class BlockListView(View, TemplateResponseMixin):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'objects': [{
                'pk': i.pk,
                'name': i.name,
                'schema': i.schema.url,
                'free_bed': i.get_free_bed(),
                'free_mat': i.get_free_mat(),
            } for i in Block.objects.all()]
        }, safe=True, content_type="application/json")


@method_decorator(login_required, name='dispatch')
class PlacementGroupListView(View, TemplateResponseMixin):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'objects': [{
                'name': i.name,
                'size': i.schema.url,
                'free': i.placement_users.count() - i.size,
                'users': [{
                    'name': u.__str__(),
                } for u in i.placement_users.all()]
            } for i in PlacementGroup.objects.all()]
        }, safe=True, content_type="application/json")


@method_decorator(login_required, name='dispatch')
class RoomListView(View, TemplateResponseMixin):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        queryset = Room.objects.all()
        if request.GET.get('block', False):
            queryset = queryset.filter(block=request.GET['block'][0])

        if request.GET.get('is_luxary', False):
            queryset = queryset.filter(is_luxary=bool(int(request.GET.get('is_luxary'))))
        else:
            queryset = queryset.filter(is_luxary=False)

        # выкидываем номера, куда уже заселились группы
        queryset = queryset.annotate(
            placement_group_count=Count(
                Subquery(
                    Order.objects.filter(room=OuterRef('pk'), placement_group__isnull=False).only('pk')[:1]
                )
            )
        ).filter(
            placement_group_count=0
        )

        return JsonResponse({
            'objects': [{
                'pk': room.pk,
                'room_number': room.room_number,
                'is_luxary': room.is_luxary,
                'bed_count': room.bed_count,
                'mat_count': room.mat_count,
                'price_on_bed': room.price_on_bed,
                'price_on_mat': room.price_on_mat,
                'free_bed': room.get_free_bed(),
                'free_mat': room.get_free_mat(),
                'placement_group_count': room.placement_group_count,
                'block': room.block.pk,
            } for room in queryset]
        }, safe=True, content_type="application/json")


@method_decorator(login_required, name='dispatch')
class OrderListView(ListView):
    model = UserOrder
    template_name = 'order/order_list.html'
    paginate_by = 1000
    ordering = ['-order__created_at']

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'term': self.request.GET.get('search', '')
        })
        return context

    def get_queryset(self, *args, **kwargs):
        user_queryset = super().get_queryset(**kwargs)
        children_queryset = ChildrenOrder.objects.all()

        if self.request.GET.get('search', None):
            term = self.request.GET.get('search', None)
            user_queryset = user_queryset.filter(
                Q(user__first_name__icontains=term) | Q(user__last_name__icontains=term) |
                Q(user__nickname__icontains=term) | Q(order__room__room_number__icontains=term) |
                Q(order__room__block__name__icontains=term)
            )

            children_queryset = children_queryset.filter(
                Q(children__name__icontains=term) | Q(order__room__room_number__icontains=term) |
                Q(order__room__block__name__icontains=term)
            )
        queryset = user_queryset.order_by(*self.get_ordering()).extra(
                select = {'is_children': 0}
        ).values(
            'is_children', 'user', 'user__nickname', 'user__first_name', 'user__last_name', 'user__city',
            'order__room__room_number', 'order__room__block__name'
        ).union(
            children_queryset.extra(
                select = {'is_children': 1}
            ).values(
                'is_children', 'children__user__id', 'children__name', 'children__user__first_name',
                'children__user__last_name', 'children__user__city', 'order__room__room_number',
                'order__room__block__name'
            )
        )
        return queryset


@method_decorator(login_required, name='dispatch')
class EatView(OrderLimitMixin, TemplateView):
    template_name = 'order/eat.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        eat_types = EatOrder.get_eat_type()
        user_eat_orders = EatOrder.objects.filter(user=self.request.user)

        for index, value in enumerate(eat_types):
            eat_types[index]['user_has'] = bool(user_eat_orders.filter(eat_type=value['name']))

        context.update({'eat_types': eat_types})
        return context

    def post(self, request, **kwargs):
        context = {}
        order = request.POST.get('eat_type', None)
        if order:
            if len(EatOrder.objects.filter(user=request.user, eat_type=order)) > 0:
                EatOrder.objects.filter(user=request.user, eat_type=order).delete()
                context.update({
                    'eat_order_status': {
                        'status': False,
                        'message': 'Мы отменили ваш заказ'
                    }
                })
                context.update(self.get_context_data(**kwargs))
                return self.render_to_response(context)

            eat_order = EatOrder(
                eat_type=order,
                user=request.user,
            )
            eat_order.save()

            context.update({
                'eat_order_status': {
                    'status': True,
                    'message': 'Вы успешно оформили заказ еды!'
                }
            })

        context.update(self.get_context_data(**kwargs))
        return self.render_to_response(context)


@method_decorator(login_required, name='dispatch')
class TransferView(OrderLimitMixin, TemplateView):
    template_name = 'order/transfer.html'

    def __update_bus_order(self, bus_pk, direction, with_child):
        bus = get_object_or_404(Bus, pk=bus_pk)

        if bus.places - len(bus.busorder_set.all()) < 1:
            return {
                'status': False,
                'message': 'К сожалению, на этом рейсе уже нет мест'
            }

        user_orders = BusOrder.objects.filter(user=self.request.user, bus__direction=direction)
        user_orders.delete()

        ticket_count = self.request.user.children_set.count() if with_child else 1
        print(ticket_count, with_child)

        for i in range(0, ticket_count):
            user_order = BusOrder(
                user=self.request.user,
                bus=bus,
            )
            user_order.save()

        return {
            'status': True,
            'message': 'Вы успешно забронировали себе место!'
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # ну ладно-ладно, от 4х запросов к БД еще никто не помирал, зато кода меньше
        context.update({
            'user_select_from': BusOrder.objects.filter(user=self.request.user, bus__direction='from_city'),
            'user_select_to': BusOrder.objects.filter(user=self.request.user, bus__direction='to_city'),

            'buses_from': Bus.objects.filter(direction='from_city').annotate(
                freeplace_count=F('places') - Count('busorder')
            ),
            'buses_to': Bus.objects.filter(direction='to_city').annotate(
                freeplace_count=F('places') - Count('busorder')
            ),
        })
        return context

    def post(self, request, **kwargs):
        bus_from = request.POST.get('bus-select-from', False)
        bus_to = request.POST.get('bus-select-to', False)
        with_child_from = request.POST.get('with-children-from', False)
        with_child_to = request.POST.get('with-children-to', False)

        if bus_from:
            bus_update_status = self.__update_bus_order(bus_from, 'from_city', with_child_from)

        if bus_to:
            bus_update_status = self.__update_bus_order(bus_to, 'to_city', with_child_to)

        context = self.get_context_data(**kwargs)
        context.update({'bus_update_status': bus_update_status})
        return self.render_to_response(context)


@method_decorator(login_required, name='dispatch')
class TransferDeleteView(DeleteView):
    model = BusOrder
    success_url = reverse_lazy('transfer')

    def delete(self, request, *args, **kwargs):
        try:
            bus_order = BusOrder.objects.get(user=request.user, pk=kwargs['pk'])
            BusOrder.objects.filter(user=request.user, bus=bus_order.bus).delete()
            return HttpResponseRedirect(self.success_url)
        except BusOrder.DoesNotExist:
            raise Http404


@method_decorator(login_required, name='dispatch')
class EventRequestListView(ListView):
    model = EventRequest
    template_name = 'order/event_request_list.html'
    paginate_by = 100

    def get_queryset(self, *args, **kwargs):
        return EventRequest.objects.filter(user=self.request.user)


@method_decorator(login_required, name='dispatch')
class EventRequestCreateView(CreateView):
    form_class = EventRequestForm
    template_name = 'order/event_request_create.html'

    def form_valid(self, form):
        order = form.save(False)
        order.user = self.request.user
        order.save()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class EventRequestEditView(UpdateView):
    form_class = EventRequestForm
    model = EventRequest
    template_name = 'order/event_request_create.html'

    def get_object(self, *args, **kwargs):
        return EventRequest.objects.get(pk=self.kwargs['id'])


@method_decorator(login_required, name='dispatch')
class EventRequestThanksView(TemplateView):
    template_name = 'order/event_request_thanks.html'
