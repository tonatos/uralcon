from django.contrib import admin
from django.db.models import Count, Subquery, OuterRef
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.utils.safestring import mark_safe
from import_export.fields import Field

from .models import (
    Block, Room, PlacementGroup,
    Order, Bus, EventRequest,
    EatOrder, BusOrder, ChildrenOrder,
    UserOrder, Membership
)
from ..users.models import User

#
#
# class OrderAdminForm(forms.ModelForm):
#     class Meta:
#         model = Order
#         fields = ('__all__')
#         widgets = {
#             'user': autocomplete.ModelSelect2(url='country-autocomplete')
#         }
#

class OrderResource(resources.ModelResource):
    total_amount = Field()
    room_amount = Field()
    eat_amount = Field()

    class Meta:
        model = Order
        fields = ('id', 'user__last_name', 'user__first_name', 'room__room_number', 'order_type', 'day_penetration_type',
                  'is_payed', 'accepted', 'created_at', 'total_amount', 'room_amount', 'eat_amount')

        export_order = ('id', 'user__last_name', 'user__first_name', 'room__room_number', 'order_type',
                        'day_penetration_type', 'is_payed', 'accepted', 'created_at', 'total_amount', 'room_amount',
                        'eat_amount')

    def dehydrate_total_amount(self, obj):
        return obj.user.get_total_amount()

    def dehydrate_room_amount(self, obj):
        return obj.user.get_room_amount()

    def dehydrate_eat_amount(self, obj):
        return obj.user.get_eat_amount()


class UserOrderResource(resources.ModelResource):
    user_place = Field()
    user_fullname = Field()

    class Meta:
        model = UserOrder
        fields = ('id', 'user_fullname', 'user__city', 'user__nickname', 'user_place', 'user__photo',)
        export_order = ('id', 'user_fullname', 'user__city', 'user__nickname', 'user_place', 'user__photo',)

    def dehydrate_user__photo(self, obj):
        return 'https://booking.ural-kon.ru%s' % (obj.user.photo.url) if obj.user.photo else ''

    def dehydrate_user_fullname(self, obj):
        return f"{obj.user.last_name} {obj.user.first_name}"

    def dehydrate_user_place(self, obj):
        place_type = 'П' if obj.place_type and 'mat' in obj.place_type  else \
                     'К' if obj.place_type and 'bed' in obj.place_type else ''

        return f"{obj.order.room.room_number if obj.order.room else ''}{place_type}"


class ChildrenOrderResource(resources.ModelResource):
    class Meta:
        model = ChildrenOrder
        fields = ('id', 'children__name', 'group', 'order__room__block__name', 'order__room__room_number', 'place_type')


class BlockAdmin(admin.ModelAdmin):
    pass


class WithoutMatPeople(admin.SimpleListFilter):
    title = 'Доплатили за отсутствие пенок'
    parameter_name = 'without_other_mat_people'

    def lookups(self, request, model_admin):
        return (
            (1, 'Да'),
            (0, 'Нет'),
        )

    def queryset(self, request, queryset):
        queryset = queryset.annotate(
            without_other_mat_people_count=Count(
                Subquery(
                    Order.objects.filter(room=OuterRef('pk'), without_other_mat_people=True).only('pk')[:1]
                )
            )
        )
        if self.value() is '1':
            return queryset.filter(without_other_mat_people_count__gt=0)
        if self.value() is '0':
            return queryset.filter(without_other_mat_people_count=0)


class RoomAdmin(admin.ModelAdmin):
    list_display = ('room_number', 'is_luxary', 'bed_count', 'mat_count', 'price_on_bed',
                    'price_on_mat', 'without_other_mat_people', 'free_place_on_bed', 'free_place_on_mat', 'human_in_room')
    list_editable = ('bed_count', 'mat_count', 'price_on_bed', 'price_on_mat',)
    list_filter = ['block', 'is_luxary', 'bed_count', 'mat_count', WithoutMatPeople]

    def without_other_mat_people(self, obj):
        return mark_safe('<img src="/static/admin/img/icon-{}.svg" alt="">'.format(
            'yes' if len(obj.order_set.filter(without_other_mat_people=True)) > 0 else 'no'
        ))
    without_other_mat_people.allow_tags = True
    without_other_mat_people.short_description = 'Без пеночников'

    def human_in_room(self, obj):
        return obj.human_in_room()
    human_in_room.short_description = 'В комнате на кроватях'

    def free_place_on_bed(self, obj):
        return obj.get_free_bed()
    free_place_on_bed.short_description = 'Св. кроватей'

    def free_place_on_mat(self, obj):
        return obj.get_free_mat()
    free_place_on_mat.short_description = 'Св. пенок'


class PlacementGroupInline(admin.StackedInline):
    model = Membership


class PlacementGroupAdmin(admin.ModelAdmin):
    inlines = [PlacementGroupInline]
    list_display = (
        '__str__', 'id', 'size', 'has_order',
    )

    def has_order(self, obj):
        return mark_safe('<img src="/static/admin/img/icon-{}.svg" alt="">'.format(
            'yes' if len(obj.order_set.all()) > 0 else 'no'
        ))
    has_order.allow_tags = True
    has_order.short_description = 'Есть заказ'


class UserOrderInline(admin.StackedInline):
    model = UserOrder
    extra = 1


class ChildrenOrderInline(admin.StackedInline):
    model = ChildrenOrder
    extra = 1


class OrderAdmin(ImportExportModelAdmin):
    resource_class = OrderResource
    list_display = (
        'id', 'user', 'room', 'order_type', 'day_penetration_type',
        'is_payed', 'accepted', 'created_at', 'room_amount',
    )
    list_filter = ['room__block', 'room__is_luxary', 'order_type', 'day_penetration_type', 'created_at', 'is_payed', 'accepted']
    inlines = [UserOrderInline, ChildrenOrderInline]
    readonly_fields = ('room_amount', )
    search_fields = ['user__email', 'user__first_name', 'user__last_name', 'user__nickname', 'room__room_number',]
    # form = OrderAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "user":
            kwargs["queryset"] = User.objects.order_by('last_name')
        return super(OrderAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def room_amount(self, obj):
        return obj.get_base_price()
    room_amount.short_description = 'Общая сумма за размещение (без учета персональных скидок)'


class BusAdmin(ImportExportModelAdmin):
    list_display = ('__str__', 'places', 'time')
    list_editable = ('places', 'time')


class EventRequestAdmin(admin.ModelAdmin):
    pass


class UserOrderAdmin(ImportExportModelAdmin):
    resource_class = UserOrderResource
    list_display = ('id', 'photo', 'user', 'nickname', 'group', 'block', 'room', 'place_type')
    list_filter = ['order__room__block', 'order__order_type']

    def photo(self, obj):
        if obj.user.photo:
            return mark_safe("<img src='/media/%s' width='100' />" % obj.user.photo)
        else:
            return ''
    photo.allow_tags = True
    photo.short_description = 'Фото'

    def block(self, obj):
        if obj.order.room:
            return obj.order.room.block
        else:
            return 'Без поселения'
    block.short_description = 'Корпус'

    def room(self, obj):
        if obj.order.room:
            return obj.order.room.room_number
        else:
            return 'Без поселения'
    room.short_description = 'Комната'

    def nickname(self, obj):
        return obj.user.nickname
    nickname.short_description = 'Никнейм'

    def group(self, obj):
        if obj.user.has_group():
            return obj.user.has_group().placement_group
        else:
            return ''
    group.short_description = 'Группа поселения'


class ChildrenOrderAdmin(ImportExportModelAdmin):
    pass


class EatOrderAdmin(ImportExportModelAdmin):
    list_display = ['id', '__str__', 'user', 'created_at']
    list_filter = ['eat_type']


class BusOrderAdmin(ImportExportModelAdmin):
    list_display = ['__str__', 'user', 'created_at']
    list_filter = ['bus__direction', 'bus__time']


admin.site.register(Order, OrderAdmin)
admin.site.register(UserOrder, UserOrderAdmin)
admin.site.register(ChildrenOrder, ChildrenOrderAdmin)
admin.site.register(EatOrder, EatOrderAdmin)
admin.site.register(BusOrder, BusOrderAdmin)

admin.site.register(Block, BlockAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(PlacementGroup, PlacementGroupAdmin)
admin.site.register(Bus, BusAdmin)
admin.site.register(EventRequest, EventRequestAdmin)