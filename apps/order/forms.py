from django import forms
from .models import EventRequest


class EventRequestForm(forms.ModelForm):
    class Meta:
        model = EventRequest
        exclude = ['user', 'is_accepted']
