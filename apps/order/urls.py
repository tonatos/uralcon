from django.urls import path

from . import views

urlpatterns = [
    path('order/alone/', views.OrderView.as_view(), name='order'),
    path('order/connect_group/', views.OrderConnectGroupView.as_view(), name='order_connect_group'),
    path('order/create_group/', views.OrderCreateGroupView.as_view(), name='order_create_group'),
    path('order/create_group/edit/<int:pk>/', views.OrderEditGroupView.as_view(), name='order_group_edit'),
    path('order/create_group/delete/<int:pk>/', views.OrderDeleteGroupView.as_view(), name='order_group_delete'),
    path('order/create_group/leave/<int:pk>/', views.OrderLeaveGroupView.as_view(), name='order_group_leave'),
    path('order/per_day/', views.OrderPerDayView.as_view(), name='order_per_day'),
    path('order/delete/<int:pk>/', views.OrderDeleteView.as_view(), name='order_delete'),
    path('order/edit/<int:pk>/', views.OrderEditView.as_view(), name='order_edit'),

    path('block-list/', views.BlockListView.as_view()),
    path('room-list/', views.RoomListView.as_view()),
    path('placement-group-list/', views.PlacementGroupListView.as_view()),

    path('order-list/', views.OrderListView.as_view(), name='order_list'),
    path('eat/', views.EatView.as_view(), name='eat'),
    path('transfer/', views.TransferView.as_view(), name='transfer'),
    path('transfer/delete/<int:pk>/', views.TransferDeleteView.as_view(), name='transfer_delete'),

    path('event-request/', views.EventRequestListView.as_view(), name='event_request'),
    path('event-request/create/', views.EventRequestCreateView.as_view(), name='event_request_create'),
    path('event-request/edit/<int:id>/', views.EventRequestEditView.as_view(), name='event_request_edit'),
    path('event-request/thanks/', views.EventRequestThanksView.as_view(), name='event_request_thanks'),
]
