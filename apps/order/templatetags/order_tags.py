import datetime

from django import template

register = template.Library()


@register.filter
def modelname(value):
    return value.__class__.__name__

@register.filter
def plus_days(value, days):
    return value + datetime.timedelta(days=int(days))

@register.simple_tag
def placement_type_checked(user, place):
    try:
        if not hasattr(user, 'age'):
            user_place_type = user.userorder_set.all()[0].place_type
        else:
            user_place_type = user.childrenorder_set.all()[0].place_type

        return 'checked="true"' if user_place_type == place else ''
    except IndexError:
        return ''
