FROM python:slim-stretch
MAINTAINER Semyachkin Vitaly <tonatossn@gmail.com>

# Ставим переменные окружения и локаль
ENV LANG=ru_RU.UTF-8 LC_ALL=ru_RU.UTF-8 LANGUAGE=ru_RU.UTF-8
ENV PYTHONBUFFERED=1 TERM=xterm-256color DEBIAN_FRONTEND=noninteractive TZ=Asia/Yekaterinburg

# Копируем файлик с зависимостями и ставим их
COPY requirements.txt /tmp/requirements.txt

# Ставим зависимости
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen && \
    echo "LANG=ru_RU.UTF-8" > /etc/default/locale && \
    echo "LC_ALL=ru_RU.UTF-8" >> /etc/default/locale && \
    echo "LANGUAGE=ru_RU.UTF-8" >> /etc/default/locale && \
    ln -s /etc/locale.alias /usr/share/locale/locale.alias && \
    apt-get update && apt-get upgrade -y && apt-get install -y locales && \
    apt-get install -y build-essential && \
    pip install pip==9 && \
    pip install --no-cache-dir -r /tmp/requirements.txt && \
    apt-get remove --purge -y build-essential  && \
    apt-get --purge autoremove -y && apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

COPY unix/docker-entrypoint.sh /usr/local/bin/
COPY unix/wait-for-it.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh /usr/local/bin/wait-for-it.sh

# Создаем рабочую директорию
RUN mkdir /opt/code
WORKDIR /opt/code

# Запускаем энтрипоинт
ENTRYPOINT ["docker-entrypoint.sh"]