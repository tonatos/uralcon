const orderUrl          = '/order/order/alone/';
const orderEditUrl      = '/order/order/edit/{{pk}}/';
const blockListUrl      = '/order/block-list/';
const roomListUrl       = '/order/room-list/';
const placementGroupUrl = '/order/placement-group-list';
const userListUrl       = '/users/children-list/';

const isDate18orMoreYearsOld = (day, month, year) => {
    return new Date(year + 18, month, day) <= new Date();
}

const showNeedBlock = (date) => {
    if ( !date ) {
        return false;
    }

    let now = new Date()
    let has18 = isDate18orMoreYearsOld( date.getDate(), date.getMonth(), date.getFullYear() )

    if ( has18  ) {
        $( '#for-parents-block' ).show();
        $( '#for-children-block' ).hide();
    } else {
        $( '#for-children-block' ).show();
        $( '#for-parents-block' ).hide();
    }
}

$( document ).ready(() => {
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $(".navbar-burger").click(() => {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

    $( '#registration-form' ).ready(() => {
        var $datepicker = $( '#id_birth_date' );

        if ($datepicker.length == 0) {
            return false;
        }

        $datepicker.datepicker({
            startDate: new Date(1990, 01, 01),
            onSelect: (fd, date) => {
                showNeedBlock( date );
            }
        });

        if ( $datepicker.val() != '') {
            let currentDate = new Date(
                $datepicker.val().split('.')[2],
                $datepicker.val().split('.')[1] - 1,
                $datepicker.val().split('.')[0]
            );

            showNeedBlock( currentDate );
            $datepicker.datepicker().data( 'datepicker' ).selectDate( currentDate );
        }

        $datepicker.on( 'change paste', (e) => {
            let inputDate = $(e.target).val();

            try {
                let selectedDate = new Date(
                    inputDate.split('.')[2], inputDate.split('.')[1] - 1, inputDate.split('.')[0]
                );
                console.log(selectedDate)
                if ( !isNaN(selectedDate) ) {
                    $datepicker.datepicker().data( 'datepicker' ).selectDate( selectedDate );
                }
            } catch (e) {
                console.log(e)
            }
        });


        var $childrenToggler = $( '#id_has_children' );
        var $childrenBlock = $( '#children-info-block' );
        var $childrenArea = $( '#id_children_info' );

        if ( $childrenArea.text() !== '' ) {
            $childrenToggler.prop('checked', true);
            $childrenBlock.show();
        }

        $childrenToggler.change((e) => {
            $childrenBlock.css('display') == 'none' ? $childrenBlock.show() : $childrenBlock.hide();
            $childrenBlock.css('display') == 'none' ? $childrenArea.text() : $childrenArea.text('');
            $childrenBlock.removeClass('hidden');
        });
    });

    $( '.js-toggle' ).each((i, obj) => {
        let $target = $( `#${$( obj ).data('target')}` );
        $( obj ).on( 'click', (e) => {
            $target.css('display') == 'none' ? $target.show() : $target.hide();
            $target.removeClass('hidden');
            return false;
        });
    });

    $( '#blocks-list' ).ready(() => {
        const $container = $( '#blocks-list' );
        const blockTeamplate = `
            <div class="media room-item">
                <div class="media-left">
                    <figure class="image is-128x128">
                        <img src="{{IMAGE}}">
                    </figure>
                </div>
                <div class="media-content">
                    <p class="title is-5">{{NAME}}</p>
                    <p>
                        Свободно {{FREE_BED}} кроватей<br>
                        Свободно {{FREE_MAT}} мест на пенках
                    </p>
                </div>
            </div>`;

        const modalTemplate = `
            <div class="modal">
                <div class="modal-background"></div>
                <div class="modal-content">
                    <p class="image">
                        <img src="{{IMAGE}}" alt="">
                    </p>
                </div>
                <button class="modal-close is-large" aria-label="close"></button>
            </div>`;

        $.get(blockListUrl, (data) => {
            $container.html('');
            data['objects'].forEach((block, i) => {
                let item = blockTeamplate
                               .replace('{{IMAGE}}', block.schema)
                               .replace('{{NAME}}', block.name)
                               .replace('{{FREE_BED}}', block.free_bed)
                               .replace('{{FREE_MAT}}', block.free_mat);
                $container.append(item);
            });
        })

        /**
          * Анимируем модалочки
          */
        $( 'body' ).on( 'click', '.room-item .image', (e) => {
            let $modal = $( modalTemplate.replace( '{{IMAGE}}', $(e.target).attr('src') ) );
            $modal
                .css({'display': 'block'})
                .find('.modal-close')
                .on( 'click', () => {
                    $modal.remove()
                })
            $( 'body' ).prepend($modal);
        }).on( 'keydown', (e) => {
            if (e.keyCode === 27) {
                $( '.modal' ).remove();
            }
        });
    });
    
    /**
      * Открываем блок пароля, когда указываем команду
      */
    $( '#connect-group-select' ).change((e) => {
        $(e.target).val()
        $( '#password-block' ).show();
    });

    /**
      * Собственно, сама форма заказа
      */
    $( '#order-room' ).ready( () => {
        const $orderRoomForm = $( '#order-room' );
        const $blockSelectBox = $( '#block-select' );
        const $roomSelectBox = $( '#room-select' );
        const $roomSelectBlock = $( '#room-select-block' );
        const $resettlementBlock = $( '#resettlement-block' );
        const $classTypeTabItem = $( '#room-type-choose li' );
        const $resettlementRadio = $( '#resettlement-block input[type=radio]');
        const $totalPriceValue = $( '#total-price-value' )
        const $withoutOtherMatPeopleBlock = $( '#without_other_mat_people-block' );
        const $withoutOtherMatPeople = $( '#without_other_mat_people' );
        const $orderErrorMessage = $( '#order-error-message' );

        const optionTpl = `<option value="{{VALUE}}">{{NAME}}</option>`;

        const resetForm = () => {
            selectedRoom = null;
            totalPrice = 0;

            $roomSelectBlock.hide();
            $resettlementBlock.hide();

            $blockSelectBox.val('');
            $resettlementBlock.find('.price:eq(0)').html('');
            $resettlementBlock.find('.price:eq(1)').html( '' );
            $resettlementRadio.prop('checked', false);
            $totalPriceValue.html( '0' );
            $orderErrorMessage.html( '' ).hide();
        }

        const showError = (er) => {
            $orderErrorMessage.html( er );
            $orderErrorMessage.show();
        }

        const getDiscount = (age) => {
            if (!age) {
                return 0;
            }
            if (age > 12 && age <= 16) {
                return discount.children_13_16;
            }
            if (age > 7 && age <= 12) {
                return discount.children_7_12;
            }
            if (age <= 7) {
                return discount.children_up_to_7_alone;
            }
            return 0;
        }

        const calculateTotalPrice = () => {
            totalPrice = 0;
            $resettlementRadio.filter(':checked').each( (i, obj) => {
                if ($( obj ).attr('value') == 'bed') {
                    totalPrice += selectedRoom.price_on_bed - getDiscount($( obj ).data('age'));
                } else if ($( obj ).attr('value') == 'mat') {
                    totalPrice += selectedRoom.price_on_mat - getDiscount($( obj ).data('age'));
                }
            });

            if ( $withoutOtherMatPeople.prop( 'checked' ) ) {
                totalPrice += withoutOtherMatPeople;
            }

            return totalPrice;
        }

        $classTypeTabItem.on( 'click', (e) => {
            if ( $( e.currentTarget ).data('class') == 'luxary' && !hasGroupOwner) {
                alert( 'Номера классы «люкс» можно бронировать только группой' );
            } else {
                roomClass = $( e.currentTarget ).data('class');
                $( '#room-type-choose li' ).removeClass('is-active');
                $( e.currentTarget ).addClass('is-active');
            }
            resetForm();
        });

        $blockSelectBox.ready( () => {
            $blockSelectBox.html('');
            $roomSelectBox.html('');

            // выбираем корпус
            $.get(blockListUrl, (data) => {
                let item = optionTpl
                               .replace('{{NAME}}', '')
                               .replace('{{VALUE}}', '---')
                $blockSelectBox.append(item);
                data['objects'].forEach((block, i) => {
                    let item = optionTpl
                                   .replace('{{NAME}}', block.name)
                                   .replace('{{VALUE}}', block.pk)
                    $blockSelectBox.append(item);
                });
            })

            // выбираем номер
            $blockSelectBox.on( 'change', (e) => {
                const blockPk = $( e.target ).val();
                $roomSelectBlock.show();
                $roomSelectBox.html('');
                selectedRoom = null;

                $.get(roomListUrl, {block: blockPk, is_luxary: roomClass == 'standart' ? 0 : 1}, (data) => {
                console.log(data['objects'].length)
                    if (data['objects'].length == 0) {
                        let item = $( optionTpl
                                       .replace('{{NAME}}', 'Свободных комнат данной категории в корпусе нет. Попробуйте выбрать другую категорию')
                                       .replace('{{VALUE}}', '---')
                                   ).attr('disabled', true);

                        $roomSelectBox.append(item);
                    } else {
                        let item = optionTpl
                                   .replace('{{NAME}}', '')
                                   .replace('{{VALUE}}', '---')
                        $roomSelectBox.append(item);

                        data['objects'].forEach((room, i) => {
                            let roomTitle = `${room.room_number} (свободно кроватей: ${room.free_bed},
                                             пенок: ${room.free_mat})`;

                            let item = $( optionTpl
                                           .replace('{{VALUE}}', room.pk)
                                           .replace('{{NAME}}', roomTitle));

                            // если нет свободных мест, значит дизаблим
                            if (!room.free_bed && !room.free_mat) {
                                item.attr('disabled', true);
                            }

                            item.attr('data-model', JSON.stringify({
                                is_luxary: room.is_luxary,
                                bed_count: room.bed_count,
                                mat_count: room.mat_count,
                                price_on_bed: room.price_on_bed,
                                price_on_mat: room.price_on_mat,
                                free_bed: room.free_bed,
                                free_mat: room.free_mat,
                            }));

                            $roomSelectBox.append(item);
                        });
                    }
                })
            });

            // выбираем расселение
            $roomSelectBox.on( 'change', (e) => {
                $resettlementBlock.show();
                let value = $( e.target ).val();
                selectedRoom = $( e.target ).find( `option[value=${value}]`).data('model');

                if ( selectedRoom.free_bed + selectedRoom.free_mat < groupCount) {
                    //alert( 'К сожалению, в этом номере недостаточно мест, чтобы разместить группу' );
                }

                $resettlementBlock.find( 'tr' ).each((i, obj) => {
                    // вот тут надо посчитать с учетом скидок
                    let age = $( obj ).find('input[type=radio]:eq(0)').data('age');
                    $( obj ).find('.price:eq(0)').html( `${selectedRoom.price_on_bed - getDiscount(age)} ₽` )

                    if (selectedRoom.price_on_mat) {
                        $( obj ).find('input[type=radio]:eq(1)').attr('disabled', false);
                        $( obj ).find('.price:eq(1)').html( `${selectedRoom.price_on_mat - getDiscount(age)} ₽` )
                    } else {
                        $( obj ).find('input[type=radio]:eq(1)').attr('disabled', true);
                        $( obj ).find('.price:eq(1)').html( 'Пенок нет' )
                    }
                });
            });

            $resettlementRadio.on( 'change', (e) => {
                $totalPriceValue.html( calculateTotalPrice() );

                if ( $resettlementRadio.filter(':checked[value=bed]').length >= selectedRoom.free_bed && roomClass == 'standart') {
                    $withoutOtherMatPeopleBlock.show();
                } else {
                    $withoutOtherMatPeopleBlock.hide();
                }
            });
            $withoutOtherMatPeople.on( 'change', (e) => {
                $totalPriceValue.html( calculateTotalPrice() );
            });

            $orderRoomForm.on( 'submit', (e) => {
                e.preventDefault();

                $orderErrorMessage.hide();

                let allCheck = $resettlementRadio.filter(':checked').length;
                let bedCheck = $resettlementRadio.filter(':checked[value=bed]').length;
                let matCheck = $resettlementRadio.filter(':checked[value=mat]').length;

                if (!selectedRoom) {
                    showError( 'Выберите комнату' );
                    return false;
                }

                $.post(instance ? orderEditUrl.replace('{{pk}}', instance) : orderUrl, $orderRoomForm.serialize(), (data) => {
                    if (data.status) {
                        location.href = orderUrl;
                    } else {
                        showError( data.message );
                    }
                    return false;
                })

                return false;
            });
        });
    });
});