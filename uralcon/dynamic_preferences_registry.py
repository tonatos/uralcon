import datetime

from dynamic_preferences.types import FloatPreference, DatePreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


prices = Section('prices')
children_contribution = Section('children_contribution')
eat = Section('eat')
registration = Section('registration')


@global_preferences_registry.register
class AllDayTicket(FloatPreference):
    section = prices
    name = 'add_day_ticket'
    default = 1500.0
    verbose_name = 'Дневная проходка «На все дни»'


@global_preferences_registry.register
class OnlySaturdayTicket(FloatPreference):
    section = prices
    name = 'only_saturday_ticket'
    default = 1000.0
    verbose_name = 'Дневная проходка «Только суббота»'


@global_preferences_registry.register
class OnlySaturdayNightTicket(FloatPreference):
    section = prices
    name = 'only_saturday_night_ticket'
    default = 600.0
    verbose_name = 'Ночная проходка «Ночь субботы»'


@global_preferences_registry.register
class PriceForAloneGroup(FloatPreference):
    section = prices
    name = 'price_for_alone_group'
    default = 800.0
    verbose_name = 'Размер доплаты за отсутствие «пеночников»'


@global_preferences_registry.register
class ChildrenUpTo7Together(FloatPreference):
    section = children_contribution
    name = 'children_up_to_7_together'
    default = 0.0
    verbose_name = 'Дети до 7 лет, которые ночуют на одной кровати с родителем'


@global_preferences_registry.register
class ChildrenUpTo7Alone(FloatPreference):
    section = children_contribution
    name = 'children_up_to_7_alone'
    default = 1500.0
    verbose_name = 'Дети до 7 лет, занимающие отдельную кровать (скидка)'


@global_preferences_registry.register
class Children7_12(FloatPreference):
    section = children_contribution
    name = 'children_7_12'
    default = 1000.0
    verbose_name = 'Дети с 7 до 12 лет (скидка)'


@global_preferences_registry.register
class Children13_16(FloatPreference):
    section = children_contribution
    name = 'children_13_16'
    default = 500.0
    verbose_name = 'Дети с 13 до 16 лет (скидка)'


@global_preferences_registry.register
class EatLunch(FloatPreference):
    section = eat
    name = 'eat_lunch'
    default = 250.0
    verbose_name = 'Цена за обед'


@global_preferences_registry.register
class EatDinner(FloatPreference):
    section = eat
    name = 'eat_dinner'
    default = 200.0
    verbose_name = 'Цена за ужин'


@global_preferences_registry.register
class RegistrationStart(DatePreference):
    section = registration
    name = 'registration_start'
    verbose_name = 'Дата начала регистрации'
    default = datetime.date(2019, 1, 20)

@global_preferences_registry.register
class RegistrationStop(DatePreference):
    section = registration
    name = 'registration_stop'
    verbose_name = 'Дата окончания регистрации'
    default = datetime.date(2019, 4, 10)