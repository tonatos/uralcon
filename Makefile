environment = develop

up:
	@make -s check
	@ln -sf conf/env.$(environment) .env

	@if [ "$(environment)" = "production" ] ; then\
	    docker-compose up --build -d;\
	    docker-compose exec back ./manage.py collectstatic --no-input;\
    else \
	    docker-compose up --build;\
	fi


check:
	@if [ "$(environment)" = "develop" ] && [ ! -f conf/env.develop ] ; then\
		echo "File develop.env doesn't exist";\
		exit 1;\
	fi

	@if [ "$(environment)" = "production" ] && [ ! -f conf/env.production ] ; then\
		echo "File production.env doesn't exist";\
		exit 1;\
	fi

migrate:
	docker-compose exec back ./manage.py makemigrations && docker-compose exec back ./manage.py migrate