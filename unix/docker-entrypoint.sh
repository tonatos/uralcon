#!/usr/bin/env bash

set -e

echo "Starting up API server of ${PROJECT_NAME} project in $ENVIRONMENT environment"

echo "Waiting for Postgres"
/usr/local/bin/wait-for-it.sh \
	--host=${POSTGRES_HOST} \
	--port=${POSTGRES_PORT} \
	--timeout=15 \
	--strict \
	-- echo "Postgres is up"

case "$ENVIRONMENT" in
	"develop" | "production")
	    ;;
	*)
		echo "Variable ENVIRONMENT has unsupported value: $ENVIRONMENT"
		exit 1
		;;
esac	

export DJANGO_SETTINGS_MODULE=uralcon.settings
echo "Variable DJANGO_SETTINGS_MODULE is set to $DJANGO_SETTINGS_MODULE value"

# Migrations should exist before building image in production mode
echo "Creating migrations"
if [ "$ENVIRONMENT" = "develop" ]; then
    python manage.py makemigrations
fi

echo "Applying migrations"
python manage.py migrate

echo "Collecting static files"
python manage.py collectstatic --no-input

echo "Starting $@"
exec $@