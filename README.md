# Uralcon

Система бронирования билетов и доп. услуг для фестиваля Uralcon.

Работает на `Django`.

## Запуск

Проект упакован в docker. Базы данных нет. Для запуска нужен только `docker` & `docker-compose`.

Developer-mode:
```
make up
```

Production-mode:
```
make environment=production up
```

Далее, смотреть в браузере по адресу: `https://0.0.0.0:8000`. проксировать туда же.

## Настройки
Настройки содержатся в файлах окружения `env.develop` и `env.production` соответственно. Список переменных:
```
PROJECT_NAME — название проекта
ENVIRONMENT — окружение (develop \ production)
``` 